#!/bin/bash
DATASET=user.${RUCIO_ACCOUNT}.dl1.hp.optimisation.configs_test
DISK=UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
rucio add-dataset ${DATASET}

UPLOADBASE=DL1_configs_hp

for i in `seq 0 0`;
do
  CONFIG=HP_config_${i}.json
  rucio upload ${CONFIG} --name ${UPLOADBASE}_${CONFIG} --rse ${DISK}
  rucio attach ${DATASET} user.${RUCIO_ACCOUNT}:${UPLOADBASE}_${CONFIG}

done
